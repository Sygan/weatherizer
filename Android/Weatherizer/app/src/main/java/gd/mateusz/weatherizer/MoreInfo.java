package gd.mateusz.weatherizer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MoreInfo extends Fragment
{
    private ArrayAdapter<String> itemsAdapter;
    private View view;

    private ListView listView;
    private String[] listData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.activity_more_info, container, false);

        listView = (ListView) view.findViewById(R.id.weatherDetailsList);

        RegisterDataRefreshedMessage();

        FillWeatherDataArray();
        PushDataToListView();

        return view;
    }

    /*** Broadcast manager ***/

    private void RegisterDataRefreshedMessage()
    {
        LocalBroadcastManager.getInstance(view.getContext()).registerReceiver(mWeatherDataChanged,
                new IntentFilter("refreshed-weather-data"));
    }

    private BroadcastReceiver mWeatherDataChanged = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            GetWeatherData();
        }
    };

    /*** View pager ***/

    public static MoreInfo newInstance()
    {
        MoreInfo f = new MoreInfo();
        Bundle b = new Bundle();

        f.setArguments(b);

        return f;
    }

    /*** Helpers ***/

    private void FillWeatherDataArray()
    {
        listData = new String[8];

        listData[0] = "Temperature: ";
        listData[1] = "Temperature max: ";
        listData[2] = "Temperature min: ";
        listData[3] = "Pressure: ";
        listData[4] = "Humidity: ";
        listData[5] = "Wind speed: ";
        listData[6] = "Wind degree: ";
        listData[7] = "Cloudiness: ";
    }

    private void GetWeatherData()
    {
        listData[0] = "Temperature: " + Math.round(MainActivity.WeatherData.temperature.getCurrent()*100)/100.0f + "°C";
        listData[1] = "Temperature max: " + Math.round(MainActivity.WeatherData.temperature.getMaximum()*100)/100.0f + "°C";
        listData[2] = "Temperature min: " + Math.round(MainActivity.WeatherData.temperature.getMinimum()*100)/100.0f + "°C";
        listData[3] = "Pressure: " + MainActivity.WeatherData.currentCondition.getPressure() + " hPa";
        listData[4] = "Humidity: " + MainActivity.WeatherData.currentCondition.getHumidity() + "%";
        listData[5] = "Wind speed: " + MainActivity.WeatherData.wind.getSpeed() + "m/s";
        listData[6] = "Wind degree: " + MainActivity.WeatherData.wind.getDeg() + "°C";
        listData[7] = "Cloudiness: " + MainActivity.WeatherData.clouds.getPercentage() + "%";

        PushDataToListView();
    }

    private void PushDataToListView()
    {
        itemsAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, listData);
        listView.setAdapter(itemsAdapter);

    }
}
