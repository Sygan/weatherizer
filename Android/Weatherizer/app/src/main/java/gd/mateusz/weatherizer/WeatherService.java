package gd.mateusz.weatherizer;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class WeatherService
{
    private static String BASE_URL = "http://api.openweathermap.org/data/2.5/weather?";
    private static String WEBAPI_KEY = "&appid=7a8dcf4966a2be53e4756fa258c9a495";

    public String getWeatherData(String cityName)
    {
        String url = BASE_URL + "q=" + cityName + WEBAPI_KEY;
        return getWeather(url);
    }

    public String getWeatherData(String latitude, String longitude)
    {
        String url = BASE_URL + "lat=" + latitude + "&lon=" + longitude + WEBAPI_KEY;
        return getWeather(url);
    }

    private String getWeather(String url)
    {
        HttpURLConnection connection = null;
        InputStream inputStream = null;

        try
        {
            connection = (HttpURLConnection) (new URL(url)).openConnection();

            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.connect();

            StringBuffer stringBuffer = new StringBuffer();
            inputStream = connection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String line = null;
            while ( (line = bufferedReader.readLine()) != null )
                stringBuffer.append(line + "\r\n");

            inputStream.close();
            connection.disconnect();

            return stringBuffer.toString();
        }
        catch (Throwable t)
        {
            MainActivity.instance.CreateErrorAlert();
            t.printStackTrace();
        }

        return null;
    }
}
spusc sie jej miedzy zeby zeby zapelnicjej ta diasteneXd
