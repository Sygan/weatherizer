package gd.mateusz.weatherizer;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainScreen extends Fragment
{
    private TextView temperatureText;
    private TextView descriptionText;
    private TextView cityText;
    private ImageView weatherImageView;
    private Button setCityButton;
    private View mainView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        SetView(inflater, container);

        GetLayoutElements();
        ConfigureSetCityButton();

        RegisterWeatherDataChangedReciver();

        return mainView;
    }

    /*** Layout ***/

    private void SetView(LayoutInflater inflater, ViewGroup container)
    {
        mainView = inflater.inflate(R.layout.activity_main_screen, container, false);
    }

    private void GetLayoutElements()
    {
        temperatureText = (TextView) mainView.findViewById(R.id.temperatureText);
        descriptionText = (TextView) mainView.findViewById(R.id.descriptionText);
        cityText = (TextView) mainView.findViewById(R.id.cityText);
        weatherImageView = (ImageView) mainView.findViewById(R.id.weatherImageView);
        setCityButton = (Button) mainView.findViewById(R.id.setCityButton);
    }

    private void ConfigureSetCityButton()
    {
        setCityButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Dialog dialog = CreateGetCityAlert(v).create();
                dialog.show();
            }
        });
    }

    private AlertDialog.Builder CreateGetCityAlert(View v)
    {
        final View setCityPopupView = LayoutInflater.from(v.getContext()).inflate(R.layout.set_city_popup, null);

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(v.getContext());
        alertBuilder.setView(setCityPopupView);

        final EditText cityNameText = (EditText) setCityPopupView.findViewById(R.id.setCityText);
        alertBuilder.setCancelable(true);

        alertBuilder.setPositiveButton("Set City", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                BroadcastNewCityNameMessage(cityNameText);
            }
        });

        return alertBuilder;
    }

    /*** Broadcast Manager ***/

    private void BroadcastNewCityNameMessage(EditText cityNameText)
    {
        Intent intent = new Intent("new-city-name");

        intent.putExtra("cityName", "" + cityNameText.getText());

        LocalBroadcastManager.getInstance(mainView.getContext()).sendBroadcast(intent);
    }

    private BroadcastReceiver mWeatherDataChanged = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            temperatureText.setText(Math.round(MainActivity.WeatherData.temperature.getCurrent()*100)/100.0f + "°C");
            descriptionText.setText(MainActivity.WeatherData.currentCondition.getDescription());
            cityText.setText(MainActivity.WeatherData.location.getCity());

            Resources res = getResources();
            int resId = res.getIdentifier("i" + MainActivity.WeatherData.currentCondition.getIcon(), "drawable", getContext().getPackageName());
            weatherImageView.setImageResource(resId);

        }
    };

    private void RegisterWeatherDataChangedReciver()
    {
        LocalBroadcastManager.getInstance(mainView.getContext()).registerReceiver(mWeatherDataChanged,
                new IntentFilter("refreshed-weather-data"));
    }

    /*** View Pager ***/

    public static MainScreen newInstance()
    {
        MainScreen f = new MainScreen();
        Bundle b = new Bundle();

        f.setArguments(b);

        return f;
    }

}
