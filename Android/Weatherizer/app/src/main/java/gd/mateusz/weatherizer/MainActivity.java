package gd.mateusz.weatherizer;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.*;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class MainActivity extends AppCompatActivity
{
    public static MainActivity instance;

    //Activity
    private static Context context;
    private ViewPager viewPager;
    private View view;

    //Weather Service
    public static Weather WeatherData;

    //Location
    private LocationManager locationManager;
    private LocationListener locationListener;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        instance = this;
        view = findViewById(android.R.id.content);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;

        SetLocationService();
        GetCurrentLocation();

        SetSwipeScreensViewPager();

        RegisterNewCityBroadcastRecivier();
    }

    /*** Location functions ***/

    private void GetWeatherForLocation(Location location)
    {
        String latitude = "" + Math.round(location.getLatitude());
        String longitude = "" + Math.round(location.getLongitude());

        JSONWeatherTask jsonWeatherTask = new JSONWeatherTask();
        jsonWeatherTask.execute(new String[]{latitude, longitude});
    }

    private void StopUpdatingLocation()
    {
        locationManager.removeUpdates(locationListener);
    }

    private void OpenLocationSettings()
    {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }

    private void OpenInternetSettings()
    {
        Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
        startActivity(intent);
    }

    private void SetLocationService()
    {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        locationListener = new LocationListener()
        {
            @Override
            public void onLocationChanged(Location location)
            {
                GetWeatherForLocation(location);
                StopUpdatingLocation();
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) { }

            @Override
            public void onProviderEnabled(String provider)
            {

            }

            @Override
            public void onProviderDisabled(String provider)
            {
                CreateEnableGPSAlert();
            }
        };
    }

    private void GetCurrentLocation()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.INTERNET}, 10);
                return;
            }
        }
        else
            GetLocation();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        switch(requestCode)
        {
            case 10:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    GetLocation();
        }
    }

    private void GetLocation()
    {
        locationManager.requestLocationUpdates("gps", 10, 0, locationListener);
    }

    /*** View Pager ***/
    private class MyPagesAdapter extends FragmentPagerAdapter
    {
        public MyPagesAdapter(FragmentManager fragmentManager)
        {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position)
        {
            switch (position)
            {
                case 0:
                    return MoreInfo.newInstance();
                case 1:
                    return MainScreen.newInstance();
            }

            return null;
        }

        @Override
        public int getCount()
        {
            return 2;
        }
    }

    private void SetSwipeScreensViewPager()
    {
        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(new MyPagesAdapter(getSupportFragmentManager()));
        viewPager.setCurrentItem(1);
    }

    /*** Broadcast Manager ***/

    private void RegisterNewCityBroadcastRecivier()
    {
        LocalBroadcastManager.getInstance(this).registerReceiver(mNewCityName,
                new IntentFilter("new-city-name"));
    }

    private BroadcastReceiver mNewCityName = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String escapedCityName = EscapeCityName(intent.getStringExtra("cityName"));

            if(escapedCityName != "")
            {
                JSONWeatherTask jsonWeatherTask = new JSONWeatherTask();
                jsonWeatherTask.execute(new String[]{escapedCityName});
            }
        }
    };

    /*** Async JSON Weather Task ***/

    private class JSONWeatherTask extends AsyncTask<String, Void, Weather>
    {
        @Override
        protected Weather doInBackground(String... params)
        {
            String data = null;
            Weather weather = new Weather();

            if(!IsInternetAvalible())
            {
                MainActivity.instance.CreateNoInternetAlert();

                this.cancel(true);
            }
            else
            {
                if (params.length == 1)
                    data = ((new WeatherService()).getWeatherData(params[0]));

                else
                    data = ((new WeatherService()).getWeatherData(params[0], params[1]));

                try
                {
                    weather = JSONParser.getWeather(data);
                } catch (JSONException e)
                {
                    MainActivity.instance.CreateErrorAlert();
                    e.printStackTrace();
                }
            }
            return weather;
        }

        @Override
        protected void onPostExecute(Weather weather)
        {
            super.onPostExecute(weather);

            MainActivity.WeatherData = weather;     //TODO: Switch it from public static if there is time.

            BroadcastWeatherRefreshedMessage();
        }

        private void BroadcastWeatherRefreshedMessage()
        {
            Intent intent = new Intent("refreshed-weather-data");

            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        }
    }

    /*** Helpers ***/

    private Boolean IsInternetAvalible()
    {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null &&  activeNetwork.isConnectedOrConnecting();
    }

    private String EscapeCityName(String cityName)
    {
        String escapedCityName = "";
        try
        {
            escapedCityName = URLEncoder.encode(cityName, "utf-8");
        }
        catch (UnsupportedEncodingException e)
        {
            MainActivity.instance.CreateErrorAlert();
            e.printStackTrace();

        }

        return escapedCityName;
    }

    /*** Error messages ***/

    public void CreateNoInternetAlert()
    {
        this.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                final View setCityPopupView = LayoutInflater.from(view.getContext()).inflate(R.layout.no_internet_popup, null);

                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(view.getContext());
                alertBuilder.setView(setCityPopupView);

                alertBuilder.setCancelable(false);

                alertBuilder.setNegativeButton("Go to settings", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        OpenInternetSettings();
                    }
                });

                Dialog dialog = alertBuilder.create();
                dialog.show();
            }
        });
    }

    public void CreateEnableGPSAlert()
    {
        this.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                final View setCityPopupView = LayoutInflater.from(view.getContext()).inflate(R.layout.gps_disabled_popup, null);

                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(view.getContext());
                alertBuilder.setView(setCityPopupView);

                alertBuilder.setCancelable(true);

                alertBuilder.setPositiveButton("Go to settings", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        OpenLocationSettings();
                    }
                });

                Dialog dialog = alertBuilder.create();
                dialog.show();
            }
        });
    }

    public void CreateErrorAlert()
    {
        this.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                final View setCityPopupView = LayoutInflater.from(view.getContext()).inflate(R.layout.error, null);

                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(view.getContext());
                alertBuilder.setView(setCityPopupView);

                alertBuilder.setCancelable(true);

                alertBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        //No operation performed.
                    }
                });

                Dialog dialog = alertBuilder.create();
                dialog.show();
            }
        });
    }


}
