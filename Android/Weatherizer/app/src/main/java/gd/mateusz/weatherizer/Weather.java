package gd.mateusz.weatherizer;

public class Weather
{
    public Location location;
    public CurrentCondition currentCondition = new CurrentCondition();
    public Temperature temperature = new Temperature();
    public Wind wind = new Wind();
    public Rain rain = new Rain();
    public Snow snow = new Snow();
    public Clouds clouds = new Clouds();

    public class CurrentCondition
    {
        private int weatherID;
        private String condition;
        private String description;
        private String icon;

        private int pressure;
        private int humidity;

        public int getWeatherID()
        {
            return weatherID;
        }
        public void setWeatherID(int weatherID)
        {
            this.weatherID = weatherID;
        }
        public String getCondition()
        {
            return condition;
        }
        public void setCondition(String condition)
        {
            this.condition = condition;
        }
        public String getDescription()
        {
            return description;
        }
        public void setDescription(String description)
        {
            this.description = description;
        }
        public String getIcon()
        {
            return icon;
        }
        public void setIcon(String icon)
        {
            this.icon = icon;
        }
        public int getPressure()
        {
            return pressure;
        }
        public void setPressure(int pressure)
        {
            this.pressure = pressure;
        }
        public int getHumidity()
        {
            return humidity;
        }
        public void setHumidity(int humidity)
        {
            this.humidity = humidity;
        }
    }

    public class Temperature
    {
        private double current;
        private double minimum;
        private double maximum;

        public double getCurrent()
        {
            return current - 272.15;
        }
        public void setCurrent(double current)
        {
            this.current = current;
        }
        public double getMinimum()
        {
            return minimum - 272.15;
        }
        public void setMinimum(double minimum)
        {
            this.minimum = minimum;
        }
        public double getMaximum()
        {
            return maximum - 272.15;
        }
        public void setMaximum(double maximum)
        {
            this.maximum = maximum;
        }
    }

    public  class Wind
    {
        private double speed;
        private double deg;

        public double getSpeed()
        {
            return speed;
        }
        public void setSpeed(double speed)
        {
            this.speed = speed;
        }
        public double getDeg()
        {
            return deg;
        }
        public void setDeg(double deg)
        {
            this.deg = deg;
        }
    }

    public  class Rain
    {
        private String time;
        private double ammount;

        public String getTime()
        {
            return time;
        }
        public void setTime(String time)
        {
            this.time = time;
        }

        public double getAmmount()
        {
            return ammount;
        }
        public void setAmmount(double ammount)
        {
            this.ammount = ammount;
        }
    }

    public  class Snow
    {
        private String time;
        private double ammount;

        public String getTime()
        {
            return time;
        }
        public void setTime(String time)
        {
            this.time = time;
        }
        public double getAmmount()
        {
            return ammount;
        }
        public void setAmmount(double ammount)
        {
            this.ammount = ammount;
        }
    }

    public  class Clouds
    {
        private int percentage;

        public int getPercentage()
        {
            return percentage;
        }

        public void setPercentage(int percentage) {
            this.percentage = percentage;
        }
    }
 }

