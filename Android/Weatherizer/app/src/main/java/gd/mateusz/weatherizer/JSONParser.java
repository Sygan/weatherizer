package gd.mateusz.weatherizer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONParser
{
    private static Weather weather = new Weather();
    private static Location location = new Location();

    private static JSONObject jsonObject;
    private static JSONArray weatherArray;
    private static JSONObject coordObject;
    private static JSONObject sysObject;
    private static JSONObject weatherObject;
    private static JSONObject mainObject;
    private static JSONObject windObject;
    private static JSONObject cloudsObject;

    public static Weather getWeather(String data) throws JSONException
    {
        CreateJSONObjectFromData(data);

        GetWeatherArray();
        GetWeatherJSONObjects();

        SetLocation();
        SetCurrentConditions();

        return weather;
    }

    private static void GetWeatherJSONObjects() throws JSONException
    {
        coordObject = jsonObject.getJSONObject("coord");
        sysObject = jsonObject.getJSONObject("sys");
        weatherObject = weatherArray.getJSONObject(0);   //There is only one element in this array
        mainObject = jsonObject.getJSONObject("main");
        windObject = jsonObject.getJSONObject("wind");
        cloudsObject = jsonObject.getJSONObject("clouds");
    }

    private static void  GetWeatherArray() throws JSONException
    {
        weatherArray = jsonObject.getJSONArray("weather");
    }

    private static void CreateJSONObjectFromData(String data) throws JSONException
    {
        jsonObject = new JSONObject(data);
    }

    private static void SetLocation() throws JSONException
    {
        location.setLatitude(coordObject.getDouble("lat"));
        location.setLongitude(coordObject.getDouble("lon"));
        location.setCity(jsonObject.getString("name"));
        location.setCountry(sysObject.getString("country"));
        location.setSunrise(sysObject.getInt("sunrise"));
        location.setSunset(sysObject.getInt("sunset"));

        weather.location = location;
    }

    private static void SetCurrentConditions() throws JSONException
    {
        weather.currentCondition.setWeatherID(weatherObject.getInt("id"));
        weather.currentCondition.setDescription(weatherObject.getString("description"));
        weather.currentCondition.setCondition(weatherObject.getString("main"));
        weather.currentCondition.setIcon(weatherObject.getString("icon"));
        weather.currentCondition.setHumidity(mainObject.getInt("humidity"));
        weather.currentCondition.setPressure(mainObject.getInt("pressure"));


        weather.temperature.setCurrent(mainObject.getDouble("temp"));
        weather.temperature.setMaximum(mainObject.getDouble("temp_max"));
        weather.temperature.setMinimum(mainObject.getDouble("temp_min"));

        weather.wind.setSpeed(windObject.getDouble("speed"));
        weather.wind.setDeg(windObject.getInt("deg"));

        weather.clouds.setPercentage(cloudsObject.getInt("all"));
    }

}
