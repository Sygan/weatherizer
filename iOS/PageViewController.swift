import Foundation
import UIKit

class PageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate
{
    var pages = [UIViewController]()
    var page1, page2, page3: UIViewController!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        SetDelegates()
        
        GetViewControllers()
        
        AppendPages()
        
        SetWeatherServiceDelegates()
    
        SetFirstView()
    }
    
    func SetDelegates()
    {
        self.delegate = self
        self.dataSource = self
    }
    
    func GetViewControllers()
    {
        page1 = storyboard?.instantiateViewControllerWithIdentifier("MoreInfoViewController")
        page2 = storyboard?.instantiateViewControllerWithIdentifier("WeatherViewController")
        //page3 = storyboard?.instantiateViewControllerWithIdentifier("ForecastViewController")
    }
    
    func AppendPages()
    {
        pages.append(page1!)
        pages.append(page2!)
        //pages.append(page3!)
    }
    
    func SetWeatherServiceDelegates()
    {
        let weatherService = WeatherService()
        
        let moreInfoTableViewController = page1 as! MoreInfoTableViewController
        weatherService.moreInfoDelegate = moreInfoTableViewController
        moreInfoTableViewController.setTables()
        
        let weatherViewController = page2 as! ViewController
        weatherViewController.weatherService = weatherService
        weatherService.delegate = weatherViewController
    }
    
    func SetFirstView()
    {
        setViewControllers([page2!], direction: UIPageViewControllerNavigationDirection.Forward, animated: true, completion: nil)
    }
    
    
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController?
    {
        let currentIndex = pages.indexOf(viewController)!
        if currentIndex != 0
        {
            let previousIndex = abs((currentIndex - 1) % pages.count)
            return pages[previousIndex]
        }
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController?
    {
        let currentIndex = pages.indexOf(viewController)!
        if currentIndex != 1
        {
            let nextIndex = abs((currentIndex + 1) % pages.count)
            return pages[nextIndex]
        }
        return nil
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int
    {
        return pages.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int
    {
        return 1
    }
}