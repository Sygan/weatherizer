import Foundation

struct Weather
{
    let CityName: String
    let Temperature: Double
    var Description: String
    let IconID: String
    let TempMax: Double
    let TempMin: Double
    let Pressure: Int
    let Humidity: Int
    let WindSpeed: Double
    let WindDegree: Int
    let Cloudiness: Int
    
    init(cityName: String, temperature: Double, description: String, iconID: String, tempMax: Double, tempMin: Double, pressure: Int, humidity: Int, windSpeed: Double, windDeg: Int, clouds: Int)
    {
        self.CityName = cityName
        self.Temperature = temperature
        self.Description = description
        
        Description.replaceRange(description.startIndex...description.startIndex, with: String(description[description.startIndex]).capitalizedString)

        self.IconID = iconID;
        self.TempMax = tempMax
        self.TempMin = tempMin
        self.Pressure = pressure
        self.Humidity = humidity
        self.WindSpeed = windSpeed
        self.WindDegree = windDeg
        self.Cloudiness = clouds
    }
    
    /* Static functions */
    
    static func CalculateKelvinsToCelsius(value: Double) -> Double
    {
        return value - 273.15
    }
    
}