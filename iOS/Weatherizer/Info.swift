//
//  Info.swift
//  Weatherizer
//
//  Created by Mateusz Pusty on 25.01.2016.
//  Copyright © 2016 Mateusz Pusty. All rights reserved.
//

import UIKit

class Info: UITableViewCell
{

    @IBOutlet weak var ContentLabel: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    
    override func setSelected(selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}
