import Foundation

protocol WeatherServiceDelegate
{
    func SetWeather(weather: Weather)
}

class WeatherService
{
    var delegate: WeatherServiceDelegate?
    var moreInfoDelegate: WeatherServiceDelegate?
    
    func GetWeatherForCity(cityName: String)
    {
        let url = NSURL(string: GenerateOpenWeatherPath(EscapeCityString(cityName)))
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithURL(url!, completionHandler: GetWeather)
        
        task.resume()
    }
    
    func GetWeatherForLocation(latitude: Double, longitude: Double)
    {
        let url = NSURL(string: GenerateOpenWeatherPath(latitude, longitude: longitude))
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithURL(url!, completionHandler: GetWeather)
        
        task.resume()
    }
    
    
    func EscapeCityString(str: String) -> String
    {
        return str.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLHostAllowedCharacterSet())!
    }
    
    func GenerateOpenWeatherPath(cityNameEscaped: String) -> String
    {
        return "http://api.openweathermap.org/data/2.5/weather?q=\(cityNameEscaped)&appid=7a8dcf4966a2be53e4756fa258c9a495"
    }
    
    func GenerateOpenWeatherPath(latitude: Double, longitude: Double) -> String
    {
        return "http://api.openweathermap.org/data/2.5/weather?lat=\(round(latitude))&lon=\(round(longitude))&appid=7a8dcf4966a2be53e4756fa258c9a495"
    }
    
    func GetWeather(data: NSData?, response: NSURLResponse?, error: NSError?) -> Void
    {
        
        let json = JSON(data: data!)
        let temp = json["main"]["temp"].double
        let desc = json["weather"][0]["description"].string
        let ico = json["weather"][0]["icon"].string
        let name = json["name"].string
        let tempMax = json["main"]["temp_max"].double
        let tempMin = json["main"]["temp_min"].double
        let humidity = json["main"]["humidity"].int
        let pressure = json["main"]["pressure"].int
        let windSpeed = json["wind"]["speed"].double
        let windDeg = json["wind"]["deg"].int
        let cloudiness = json["clouds"]["all"].int
        
        let weather = Weather(cityName: name!, temperature: temp!, description: desc!, iconID: ico!, tempMax: tempMax!, tempMin: tempMin!, pressure: pressure!, humidity: humidity!, windSpeed: windSpeed!, windDeg: windDeg!, clouds: cloudiness!)
        
        if self.delegate != nil
        {
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.delegate?.SetWeather(weather)
            })
        }
        
        if self.moreInfoDelegate != nil
        {
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.moreInfoDelegate?.SetWeather(weather)
            })
        }
    }
    
}