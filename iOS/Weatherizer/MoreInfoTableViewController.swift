//
//  MoreInfoTableViewController.swift
//  Weatherizer
//
//  Created by Mateusz Pusty on 25.01.2016.
//  Copyright © 2016 Mateusz Pusty. All rights reserved.
//

import Foundation
import UIKit

class MoreInfoTableViewController: UITableViewController, WeatherServiceDelegate
{
    var weatherData = [String]()
    //var weatherService: WeatherService!
    
    func setTables()
    {
        weatherData.append("Temperature: " )
        weatherData.append("Temperature min: ")
        weatherData.append("Temperature max: ")
        weatherData.append("Pressure: ")
        weatherData.append("Humidity: ")
        weatherData.append("Wind speed: ")
        weatherData.append("Wind degree: ")
        weatherData.append("Cloudiness: ")
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return weatherData.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cellIdentifier = "InfoTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! Info
        
        cell.ContentLabel.text = weatherData[indexPath.row]
        
        
        
        return cell
    }
    
    
    func SetWeather(weather: Weather)
    {
        weatherData[0] = ("Temperature: \(round(Weather.CalculateKelvinsToCelsius(weather.Temperature)*1000)/1000)°C" )
        weatherData[1] = ("Temperature min: \(round(Weather.CalculateKelvinsToCelsius(weather.TempMin)*1000)/1000)°C")
        weatherData[2] = ("Temperature max: \(round(Weather.CalculateKelvinsToCelsius(weather.TempMax)*1000)/1000)°C")
        weatherData[3] = ("Pressure: \(weather.Pressure) hPa")
        weatherData[4] = ("Humidity: \(weather.Humidity) %")
        weatherData[5] = ("Wind speed: \(weather.WindSpeed) m/s")
        weatherData[6] = ("Wind degree: \(weather.WindDegree)°C")
        weatherData[7] = ("Cloudiness: \(weather.Cloudiness) %")
        
        self.tableView.reloadData()
    }
}