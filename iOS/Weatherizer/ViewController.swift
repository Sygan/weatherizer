import UIKit
import CoreLocation


class ViewController: UIViewController, WeatherServiceDelegate
{
    /* Variables */
    var weatherService: WeatherService!
    var locationManager: LocationManager!
    var location: CLLocation!
    var updateToGPS = true
    
    /* IBOutlets */
    @IBOutlet weak var TemperatureLabel: UILabel!
    @IBOutlet weak var DescriptionLabel: UILabel!
    @IBOutlet weak var CityLabel: UILabel!
    @IBOutlet weak var WeatherImage: UIImageView!
    
    var Alert: UIAlertController!
    
    /* Actions */
    
    @IBAction func SetCityAction(sender: UIButton)
    {
        OpenCityAlert()
    }
     
    /* Choose city alert */

    func OpenCityAlert()
    {
        Alert = CreateAlertController()
        
        Alert.addAction(CreateCancelButton())
        
        Alert.addAction(CreateOKButton())
        
        Alert.addTextFieldWithConfigurationHandler(CreateAlertTextField)
        
        self.presentViewController(Alert, animated: true, completion: nil)
    }
    
    func OpenRefreshedWeatherAlert()
    {
        let refreshed = UIAlertController(title: "Refreshed", message: "Weather has been refreshed", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        self.presentViewController(refreshed, animated: true, completion: nil)
        
        refreshed.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func CreateAlertTextField(textField: UITextField) -> Void
    {
        textField.placeholder = "City Name"
    }

    func CreateAlertController() -> UIAlertController
    {
        return  UIAlertController(title: "City", message: "Enter city name: ", preferredStyle: UIAlertControllerStyle.Alert)
    }
    
    func CreateCancelButton() -> UIAlertAction
    {
        return UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
    }
    
    func CreateOKButton() -> UIAlertAction
    {
        return UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: ButtonOKPressed)
    }
    
    func ButtonOKPressed(action: UIAlertAction) -> Void
    {
        let textField = Alert.textFields?[0]
        
        let cityName = textField?.text
        
        if(cityName != "")
        {
            weatherService.GetWeatherForCity(cityName!)
        }
    }
    
    /* Swipe handling */
    
    func SetLeftSwipe()
    {
        let upSwipe = UISwipeGestureRecognizer(target: self, action: Selector("handleSwipes:"))
        
        upSwipe.direction = .Up
        
        view.addGestureRecognizer(upSwipe)
    }
    
    func handleSwipes(sender: UISwipeGestureRecognizer)
    {
        if(sender.direction == .Up)
        {
            RefreshWeatherToLocation()
        }
    }
    
    /* Weather Service */
    
    func RefreshWeatherToLocation()
    {
        updateToGPS = true
        
        locationManager.locationManager.startUpdatingLocation()
        
    }
    
    func SetLocatonManager()
    {
        locationManager = LocationManager()
        
        SetLocationAvalibleNSNotification()
    }
    
    /* NSNotification */
    
    func SetLocationAvalibleNSNotification()
    {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "locationAvalible", name: newLocationAvalibleKey, object: nil)
    }
    
    func locationAvalible()
    {
        let location = locationManager.GetLastLocation()
        
        if updateToGPS == true
        {
            weatherService.GetWeatherForLocation(location.coordinate.latitude, longitude: location.coordinate.longitude)
            updateToGPS = false
            
            OpenRefreshedWeatherAlert()
        }
    }
    
    
    /* Overided functions */
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        SetLocatonManager()
        
        SetLeftSwipe()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    
    /* Weather Service Delegate */
    
    func SetWeather(weather: Weather)
    {
        CityLabel.text = weather.CityName
        
        let celsius = Weather.CalculateKelvinsToCelsius(weather.Temperature)
        
        TemperatureLabel.text = "\(round(100*celsius)/100)°C"
        
        DescriptionLabel.text = weather.Description
        
        WeatherImage.image = UIImage(named: "\(weather.IconID).png")
    }
}

