import Foundation
import CoreLocation

let newLocationAvalibleKey = "pl.sygan.newLocationAvalibleKey"

class LocationManager: NSObject, CLLocationManagerDelegate
{
    
    var locationManager: CLLocationManager = CLLocationManager()
    var location: CLLocation!
    
    override init()
    {
        super.init()
        
        self.locationManager.delegate = self
        self.locationManager.distanceFilter = 3000
        self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        self.locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() == false
        {
            print("Is disabled!")
        }
    
    }

    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus)
    {
        print("didChangeAuthorizationStatus")
        
        switch status
        {
            case .NotDetermined:
                print(".NotDetermined")
                break
            
            case .Authorized:
                print(".Authorized")
                self.locationManager.startUpdatingLocation()
                break
        
            case .Denied:
                print(".Denied")
                break
            
            default:
                print("Unhandled authorization status")
                break
        }
        
    }

    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        locationManager.stopUpdatingLocation()
        
        location = locations.last! as CLLocation
        
        NSNotificationCenter.defaultCenter().postNotificationName(newLocationAvalibleKey, object: self)
        
    }
    
    func GetLastLocation() -> CLLocation
    {
        return location
    }
}
